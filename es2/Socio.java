import java.util.LinkedList;
import java.util.ListIterator;


public abstract class Socio {
	static LinkedList<Volo> voli = new LinkedList<Volo>();
	int migliaVolate;
	int bonus;
	
	public Socio(){
//		migliaVolate;
//		migliaTotali;
	}
	
	public int getMigliaVolate() {
		return this.migliaVolate;
	}
	
	public void addMiglia(int migliaThisVolo){
		migliaVolate = getMigliaVolate()+ migliaThisVolo;
		System.out.println("Aggiornamento miglia volate:\t"+getMigliaVolate());
	}
	public int getMigliaTot() {
		return this.migliaVolate*getBonus();
	}
	public int getBonus(){
		return bonus;
	}
	public void setBonus(int n){
		bonus = n;
	}
	public void addVolo(int numBiglietto, String tratta, int miglia){
		//aggiunge alla linked list dei voli un nuovo volo
		voli.add(new Volo(numBiglietto, tratta, miglia));
		addMiglia(miglia);
		System.out.println("Punti Miglia aggiornati:\t"+this.getMigliaTot()+"\n");
	}

	public void visualizzaVoli() {
		System.out.println("Inizio routine visualizzazione voli\n");
		ListIterator<Volo> iterator = voli.listIterator();
		while(iterator.hasNext()){
			Volo questo = iterator.next();
			System.out.println("VOLO #"+questo.biglietto+"\ntratta: "+questo.tratta+"\nmiglia: "+questo.miglia+"\n");
		}
		System.out.println();
	}

	public void visualizzaPunti() {
		System.out.println("Visualizzazione punti\n" +
				"Il socio ha totalizzato un totale di " + getMigliaTot() + "miglia");

	}

	public void visualizzaMiglia() {
		System.out.println("Visualizzazione miglia acquistate\n" +
				"Il socio ha totalizzato un totale di " + getMigliaVolate() + "miglia");
	}

	public boolean controlloPuntiEu() {
		return this.getMigliaTot()>20000;
	}
	public boolean controlloPuntiW() {
		return this.getMigliaTot()>45000;
	}

	public void sottrai(int migliaDaSottrarre) {
		setMigliaVolate(getMigliaVolate()-migliaDaSottrarre);
	}

	private void setMigliaVolate(int i) {
		this.migliaVolate=i;
	}
}

