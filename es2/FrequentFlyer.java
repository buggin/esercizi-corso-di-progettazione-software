import java.io.Console;
import java.io.IOException;
import java.util.Hashtable;


public class FrequentFlyer {

	static Hashtable<Integer, Socio> id = new Hashtable<Integer, Socio>();

	public static void main (String args[]) throws IOException{
		Console c = System.console();
		if (c==null){
			System.err.println("No console available");
		}
		do{
			System.out.println("i = inserisci socio\n" +
					"e = elimina socio\n" +
					"g = gestisci socio\n" +
					"q = esci(e perdi tutte le modifiche)");
			char s = carattereSelezione(c);
			int socio = 0;
			switch(s){
			case('i'):{
				inserisciNuovoSocio();
				break;
			}
			case('e'):{
				socio = selezionaSocio(c);
				eliminaSocio(socio);
				break;
			}

			case('g'):{
				socio = selezionaSocio(c);
				gestisciSocio(socio, c);
				break;
			}
			case('q'):{
				return;
			}
			}	
		}while(true);
	}
	private static char carattereSelezione(Console c) {
		String sel = c.readLine();
		while(sel.isEmpty()||sel.length()>1){
			System.err.println("inserisci solo un carattere");
			sel = c.readLine();
			if(sel.isEmpty()||!Character.isLetter(sel.charAt(0)))
				sel = "";
		}
		return sel.charAt(0);
	}
	private static void inserisciNuovoSocio() {
		int size=id.size()+1;
		id.put(size,new Ordinario());
		System.out.println("creato socio ordinario numero "+size);
	}
	private static void eliminaSocio(int socio) {
		System.out.println("\nroutine eliminazione\n");
		id.remove(socio);
		System.out.println("eliminato socio numero "+socio);
	}
	private static int selezionaSocio(Console c) {
		int socio = 0;
		Socio soc = null;
		while(socio==0||soc == null){
			System.out.println("inserisci un numero socio valido");
			socio = Integer.parseInt(c.readLine());
			soc=id.get(socio);
		}
		return socio;
	}
	private static void gestisciSocio(int socioId, Console c) {
		Socio socio = id.get(socioId);
		do{
			System.out.println("inizio routine gestione utente "+socioId+"\n" +
					"i = inserisci volo\n" +
					"a = aggiungi volo premio\n" +
					"v = visualizza voli\n" +
					"p = visualizza punti\n" +
					"m = visualizza miglia\n" +
					"q = esci");
			char s = carattereSelezione(c);
			switch(s){
			case('i'):{
				controlloInserimento(c,socioId);
				break;
			}
			case('q'):{
				return;
			}
			case('v'):{
				socio.visualizzaVoli();
				break;
			}
			case('p'):{
				socio.visualizzaPunti();
				break;
			}
			case('m'):{
				socio.visualizzaMiglia();
				break;
			}
			case('a'):{
				nuovoVoloPremio(c,socio,socioId);
			}
			}
		}while(true);
	}
	private static void nuovoVolo(int socioId, int numBiglietto, String tratta, int miglia) {
		System.out.println("inizio routine nuovo volo\n");
		Socio socio = id.get(socioId);
		socio.addVolo(numBiglietto,tratta,miglia);
	}
	private static void nuovoVoloPremio(Console c, Socio socio, int socioId){
		System.out.println("\nRoutine Volo premio\n");
		System.out.println("e = localita\' in Europa\n" +
				"w = altra");
		char s2 = carattereSelezione(c);
		switch(s2){
			case('e'):{
				if(socio.controlloPuntiEu()){
					System.out.println("E\' possibile effettuare una prenotazione con destinazione europea");
					int migliaDaSottrarre = controlloInserimento(c, socioId);
					socio.sottrai(migliaDaSottrarre);
				}
				else{
					System.out.println("NON e\' possibile effettuare una prenotazione con destinazione europea");
				}
				break;
			}
			case('w'):{
				if(socio.controlloPuntiW()){
					System.out.println("E\' possibile effettuare una prenotazione con destinazione europea");
					int migliaDaSottrarre = controlloInserimento(c, socioId);
					socio.sottrai(migliaDaSottrarre);
				}
				else{
					System.out.println("NON e\' possibile effettuare una prenotazione con destinazione europea");
				}
				break;
			}
		}
		
	}
	private static int controlloInserimento(Console c, int socioId) {
		int numeroBiglietto = 0;
		String biglietto = null;
		String tratta = null;
		int miglia = 0;
		String migl = null;
		while(biglietto==null||!biglietto.matches("\\d")){
			System.out.println("inserisci numero biglietto");
			biglietto = c.readLine();
			if(!biglietto.matches("\\d")){
				numeroBiglietto=0;
				biglietto=null;
				System.out.println("biglietto non valido");
			}
			else{
				numeroBiglietto = Integer.parseInt(biglietto);
			}
		}
		while(tratta==null){
			System.out.println("inserisci tratta");
			tratta = c.readLine();
		}
		while(migl==null){
			System.out.println("inserisci miglia");
			migl = c.readLine();
			if(!migl.matches("\\d")){
				migl = null;
				System.out.println("le miglia sono un numero");
			}
			else{
				miglia = Integer.parseInt(migl);
			}
		}
		nuovoVolo(socioId,numeroBiglietto,tratta,miglia);
		return miglia;
	}
}
